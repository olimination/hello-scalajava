package org.hello;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class GreetingJavaTest {

	@Test
	public void testHelloWorld() {
		GreetingJava testee = new GreetingJava();
		assertEquals(testee.getText(), "Hello World");
	}

}
