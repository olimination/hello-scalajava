package org.hello

import scala.collection.mutable.Stack
import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.FlatSpec

class GreetingSpec extends FlatSpec {

  "The GreetingScala" should "return Hello World from Java through Scala" in {
    val testee = GreetingScala
    assert(testee.text == "Hello World")
  }

  "A Stack" should "pop values in last-in-first-out order" in {
    val stack = new Stack[Int]
    stack.push(1)
    stack.push(2)
    assert(stack.pop() === 2)
    assert(stack.pop() === 1)
  }

  it should "throw NoSuchElementException if an empty stack is popped" in {
    val emptyStack = new Stack[String]
    intercept[NoSuchElementException] {
      emptyStack.pop()
    }
  }
}