package org.hello

object GreetingScala {

  def listArgs(x: Array[String]) = x.foldLeft("")((a, b) => a + b)

  def main(args: Array[String]) {
    println("Hello World in Scala, we say now Hello from Java: ")
    val greetJava = new GreetingJava
    greetJava.greet()
    println("concat arguments = " + listArgs(args))
  }

  def text = "Hello World"

}
