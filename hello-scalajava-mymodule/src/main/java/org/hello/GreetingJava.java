package org.hello;

public class GreetingJava {
	
	public void greet() {
		System.out.println("Hello World from Java");
	}
	
	public String getText() {
		return "Hello World";
	}
	
}
