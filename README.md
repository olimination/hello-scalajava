# Scala/Java Maven Example Project

This is an example Maven webapp project for mixed Scala/Java sources.

## Features

* Supports Scala sources (src/main/scala, src/test/scala)
* Supports Java sources
* Supports proper Eclipse project file and classpath generation
* Supports scalatest Maven plugin to execute the ScalaTest tests with Maven command line
* Supports running as webapp with the Jetty plugin
* Supports an Integration testing module for executing ScalaTest Selenium DSL 
integration tests (by the Tomcat7 Maven plugin)


## Building the project

**Build and test**

`root/> mvn install`


**Build, test and execute integration tests**

`root/> mvn install -P integration`


**Only start the webapp with Jetty**

`root/hello-scalajava-webapp/> mvn jetty:run`
