<?xml version="1.0"?>
<project
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd"
	xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<modelVersion>4.0.0</modelVersion>
	<parent>
		<groupId>org.hello</groupId>
		<artifactId>hello-scalajava-parent</artifactId>
		<version>1.0-SNAPSHOT</version>
		<relativePath>../hello-scalajava-parent</relativePath>
	</parent>
	<artifactId>hello-scalajava-integration</artifactId>
	<packaging>war</packaging>
	<name>Hello ScalaJava - Integration</name>

	<dependencies>
		<dependency>
			<groupId>org.hello</groupId>
			<artifactId>hello-scalajava-webapp</artifactId>
			<version>${project.version}</version>
			<type>war</type>
		</dependency>
		<dependency>
			<groupId>org.scala-lang</groupId>
			<artifactId>scala-library</artifactId>
		</dependency>

		<!-- Test -->
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
		</dependency>
		<dependency>
			<groupId>org.scalatest</groupId>
			<artifactId>scalatest_2.10</artifactId>
		</dependency>
		<dependency>
			<groupId>org.seleniumhq.selenium</groupId>
			<artifactId>selenium-java</artifactId>
		</dependency>
	</dependencies>

	<build>
		<finalName>hello-scalajava</finalName>

		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-war-plugin</artifactId>
				<version>2.3</version>
				<configuration>
					<overlays>
						<overlay>
							<groupId>org.hello</groupId>
							<artifactId>hello-scalajava-webapp</artifactId>
						</overlay>
					</overlays>
				</configuration>
			</plugin>

			<!-- Scala Maven plugin -->
			<plugin>
				<groupId>net.alchim31.maven</groupId>
				<artifactId>scala-maven-plugin</artifactId>
				<executions>
					<execution>
						<id>scala-compile-first</id>
						<phase>process-resources</phase>
						<goals>
							<goal>add-source</goal>
							<goal>compile</goal>
						</goals>
					</execution>
					<execution>
						<id>scala-test-compile</id>
						<phase>process-test-resources</phase>
						<goals>
							<goal>testCompile</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
			</plugin>

			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>build-helper-maven-plugin</artifactId>
				<executions>
					<!-- Add src/main/scala to source path of Eclipse -->
					<execution>
						<id>add-source</id>
						<phase>generate-sources</phase>
						<goals>
							<goal>add-source</goal>
						</goals>
						<configuration>
							<sources>
								<source>src/main/scala</source>
							</sources>
						</configuration>
					</execution>

					<!-- Add src/test/scala to test source path of Eclipse -->
					<execution>
						<id>add-test-source</id>
						<phase>generate-sources</phase>
						<goals>
							<goal>add-test-source</goal>
						</goals>
						<configuration>
							<sources>
								<source>src/test/scala</source>
							</sources>
						</configuration>
					</execution>
				</executions>
			</plugin>

			<!-- Generate Eclipse artifacts for projects mixing Scala and Java -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-eclipse-plugin</artifactId>
				<configuration>
					<downloadSources>true</downloadSources>
					<downloadJavadocs>true</downloadJavadocs>
					<projectnatures>
						<projectnature>org.scala-ide.sdt.core.scalanature</projectnature>
						<projectnature>org.eclipse.jdt.core.javanature</projectnature>
					</projectnatures>
					<buildcommands>
						<buildcommand>org.scala-ide.sdt.core.scalabuilder</buildcommand>
					</buildcommands>
					<classpathContainers>
						<classpathContainer>org.scala-ide.sdt.launching.SCALA_CONTAINER</classpathContainer>
						<classpathContainer>org.eclipse.jdt.launching.JRE_CONTAINER</classpathContainer>
					</classpathContainers>
					<excludes>
						<!-- in Eclipse, use scala-library, scala-compiler from the SCALA_CONTAINER 
							rather than POM <dependency> -->
						<exclude>org.scala-lang:scala-library</exclude>
						<exclude>org.scala-lang:scala-compiler</exclude>
					</excludes>
					<sourceIncludes>
						<sourceInclude>**/*.scala</sourceInclude>
					</sourceIncludes>
				</configuration>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<configuration>
					<useFile>true</useFile>
					<disableXmlReport>true</disableXmlReport>
					<includes>
						<include>**/*Test.*</include>
					</includes>
				</configuration>
			</plugin>

		</plugins>
	</build>

	<profiles>
		<profile>
			<id>integration</id>
			<activation>
				<activeByDefault>false</activeByDefault>
			</activation>
			<build>
				<finalName>hello-scalajava</finalName>

				<plugins>
					<plugin>
						<groupId>org.apache.tomcat.maven</groupId>
						<artifactId>tomcat7-maven-plugin</artifactId>
						<version>2.0</version>
						<configuration>
							<path>/</path>
							<contextFile>${basedir}/src/test/resources/tomcat/context.xml</contextFile>
							<useTestClasspath>true</useTestClasspath>
						</configuration>
						<executions>
							<!-- At pre-integration-test phase, run the war in an embedded Tomcat 
								server. -->
							<execution>
								<id>start-tomcat</id>
								<phase>pre-integration-test</phase>
								<goals>
									<goal>run-war-only</goal>
								</goals>
								<configuration>
									<port>8080</port>
									<fork>true</fork>
								</configuration>
							</execution>
							<!-- At post-integration-test phase, stop the embedded Tomcat server. -->
							<execution>
								<id>stop-tomcat</id>
								<phase>post-integration-test</phase>
								<goals>
									<goal>shutdown</goal>
								</goals>
							</execution>
						</executions>
					</plugin>

					<!-- ScalaTest plugin -->
					<plugin>
						<groupId>org.scalatest</groupId>
						<artifactId>scalatest-maven-plugin</artifactId>
						<executions>
							<execution>
								<id>scalatest-integration-test</id>
								<phase>integration-test</phase>
								<goals>
									<goal>test</goal>
								</goals>
							</execution>
						</executions>
						<configuration>
							<!-- tell the plugin where your classes are -->
							<runpath>target/test-classes,target/classes</runpath>

							<!-- this just finds all Suites in your package of your test-classes -->
							<wildcards>org.hello</wildcards>

							<!-- "W" means "Without color", it removes the ANSI-color-chars from 
								the output -->
							<stdout>WD</stdout>
						</configuration>
					</plugin>
				</plugins>
			</build>
		</profile>

	</profiles>

</project>
