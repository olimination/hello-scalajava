package org.hello

import org.scalatest.selenium.HtmlUnit
import org.scalatest.matchers.ShouldMatchers
import org.scalatest.FlatSpec

class HomepageSpec extends FlatSpec with ShouldMatchers with HtmlUnit {

  val host = "http://localhost:8080"
    
  "The Hello ScalaJava homepage" should "have the correct title" in {
    go to (host + "/index.jsp")
    title should be("Hello - ScalaJava - Webapp")
  }
  
    "The Hello ScalaJava homepage" should "have the correct paragraph text" in {
    go to (host + "/index.jsp")
    val ele: Option[Element] = find(tagName("h1"))
    
    ele.get.text should include ("Hello World with Scala and Java")
  }
}